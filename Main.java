package collectionOfPeople;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    public static List<Human> people = new ArrayList<>();

    public static void main(String[] args) {


        selectOption();


    }

    public static void selectOption() {
        int option;
        do {
            printMenu();
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    addHuman();
                    break;
                case 2:
                    removeHuman();
                    break;
                case 3:
                    checkIfTheyAreEqual();
                    break;
                case 4:
                    displayTheListOfPeple();
                    break;
                case 0:
                    exit();
                    break;

            }
        } while (option != 0);
    }

    public static void addHuman() {
        scanner.nextLine();
        System.out.println("Enter name: ");
        String name = scanner.nextLine();
        System.out.println("Enter surname: ");
        String surname = scanner.nextLine();
        System.out.println("Enter age: ");
        int age = scanner.nextInt();

        Human human = new Human(name, surname, age);
        people.add(human);

    }

    public static void removeHuman() {
        displayTheListOfPeple();
        System.out.println("Which person do you want to remove?");
        System.out.println("\nExit by choose 0");
        int choose = scanner.nextInt();
        if(choose != 0){
            people.remove(choose - 1);
            System.out.println("You delete person number " + choose + " from the list!");
            displayTheListOfPeple();
        }else System.out.println("You exit!");


    }

    public static void checkIfTheyAreEqual() {
        System.out.println("Choose index people from the list to compare.");
        System.out.println("Index number 1: ");
        int indexHuman1 = scanner.nextInt();
        System.out.println("Index number 2: ");
        int indexHuman2 = scanner.nextInt();
        Human human1 = people.get(indexHuman1);
        Human human2 = people.get(indexHuman2);
        if(human1.equals(human2)){
            System.out.println("Human1 equals human2");
        }else System.out.println("Human1 is not equals Human2");
    }

    public static void displayTheListOfPeple() {
        System.out.println("List of people: ");
        for (int i = 0; i < people.size(); i++) {
            System.out.println(i + 1 + " " + people.get(i));

        }
    }

    public static void exit() {
        System.out.println("The end");
    }

    public static void printMenu() {
        System.out.println("Choose option: \n 1.Add Human \n 2.Remove Human" +
                "\n 3.Check if they are equal \n 4.Display the list of people \n 0.Exit menu");

    }
}
